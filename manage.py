#!/usr/bin/env python
import os
import sys

sys.dont_write_bytecode = True

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "movieforme.settings")
    from django.core.management import execute_from_command_line
    # When you are testing, it gets boring
    # typing sudo python manage.py runserver 0.0.0.0:80
    # and bash scripts suck, so we just use sudo manage.py run
    
    # Careful though, this is bound on all addresses
    # Run as sudo.
    if len(sys.argv) > 1 and sys.argv[1] == "run":
        sys.argv.remove("run")
        sys.argv+= ["runserver", "0.0.0.0:80"]

    execute_from_command_line(sys.argv)
