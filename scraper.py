import sys, traceback
sys.dont_write_bytecode = True

from bs4 import BeautifulSoup
import requests, urlparse, django, os, re, random, urllib
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "movieforme.settings")
django.setup()
from api.models import *

from multiprocessing.dummy import Pool

class GenreScrape:
    def __init__(self):
        self.pool = Pool(4)
        
    def load(self, url, **data):
        if not "headers" in data: data["headers"] = {}
        data["headers"]["User-Agent"] = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Firefox/32.0"
        return BeautifulSoup(requests.get(url, **data).text)
        
    def download_image(self, id, url):
        path = os.path.dirname(os.path.abspath(__file__))
        urllib.urlretrieve(url, "%s/static/movie_posters/%i.jpg" % (path, id))
        
    def parse_imdb_id(self, link):
        return re.search(r"[a-z0-9]{6,}", link).group(0)
        
    def parse_movie(self, mv_info):
        imdb_id = mv_info["imdb_id"]
        document = self.load("http://imdb.com/title/%s/" % imdb_id)
        movie_name = document.select(".header .itemprop")[0].text.encode("utf8")
        try:
            release_year = int(document.select(".header .nobr a")[0].text.encode("utf8"))
        except:
            release_year = None
        try:
            rating = float(document.select("div.titlePageSprite")[0].text.encode("utf8"))
        except:
            rating = 5
        try:
            image_url = document.select("#img_primary img")[0]["src"].encode("utf8")
        except:
            image_url = None
        result = {
            "imdb_id": imdb_id,
            "name": movie_name,
            "release_year":  release_year,
            "rating":  rating,
            "image_url": image_url,
            "related": []
        }
        if not "parent" in mv_info:
            for related in document.select(".rec_details"):
                related_link = related.select(".rec-title a")[0]["href"].encode("utf8")
                related_id = self.parse_imdb_id(related_link)
                result["related"].append(related_id)
        return result
        
    def try_save(self, parsed_movie):
        if Movie.objects.filter(imdb_id = parsed_movie["imdb_id"]).count() != 0:
            return
        movie_details = dict(parsed_movie)
        movie_details.pop("related")
        movie_details.pop("image_url")
        movie = Movie(**movie_details)
        movie.save()
        return movie
        
    def scrape(self, genre):
        for n in range(0, 1):
            document = self.load("http://www.imdb.com/search/title", params = {
                "count": "250",
                "view": "simple",
                "genres": genre,
                "sort": "moviemeter",
                "title_type": "feature"
            })
            scrape_queue = []
            for movie in document.select("#main .results tr"):
                if(len(movie.select("th"))) > 0: continue # this is not a movie row
                link = movie.select(".title a")[0]["href"].encode("utf-8")
                scrape_queue.append({"imdb_id": self.parse_imdb_id(link)})
            
            related_queue = []
            for parsed_movie in self.pool.map(self.parse_movie, scrape_queue):
                self.try_save(parsed_movie)
                for related_id in parsed_movie["related"]:
                    related_queue.append({"imdb_id": related_id, "parent": parsed_movie["imdb_id"]})
                
            for parsed_related in self.pool.map(self.parse_movie, related_queue):
                print(parsed_related["name"])

if __name__ == "__main__":
    sc = GenreScrape()
    #for genre in ["action", "adventure", "animation", "biography", "comedy", "crime", "drama",
    #"family", "fantasy", "film_noir", "history", "horror", "music", "musical", "mystery",
    #"romance", "sci_fi", "short", "sport", "thriller", "war", "western"]:
    genre = sys.argv[1]
    sc.scrape(genre)
