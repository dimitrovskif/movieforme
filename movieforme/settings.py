#===== Paths =====#
import os
PROJECT_DIR = os.path.dirname(__file__)
BASE_DIR = os.path.dirname(PROJECT_DIR)

#===== Database =====#
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'movieforme',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '127.0.0.1'
    }
}

#===== Security =====#
SECRET_KEY = 'weqrcwqerc'


#===== Misc =====#
DEBUG = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS = []

#===== Apps =====#
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'api',
)
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
SESSION_ENGINE = "django.contrib.sessions.backends.file"
ROOT_URLCONF = 'movieforme.urls'
WSGI_APPLICATION = 'movieforme.wsgi.application'

#===== Languages =====#
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


#===== Global static stuff =====#
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, "static"),
    os.path.join(PROJECT_DIR, "movie_posters"),
)

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, "templates"),
)