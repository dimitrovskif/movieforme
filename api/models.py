from django.db import models

class Movie(models.Model):
    name = models.CharField(max_length = 256)
    release_year = models.IntegerField(null = True)
    rating = models.FloatField(null = True)
    imdb_id = models.CharField(max_length = 10, unique = True)
    users_liking = models.ManyToManyField("User", through = "UserLike")

    def decorated_name(self):
        name_template = "%s (%i)"
        return name_template % (self.name, self.release_year)

    def image(self):
        url_template = "%i.jpg"
        return url_template % self.id
        
    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "decorated_name": self.decorated_name(),
            "rating": self.rating,
            "release_year": self.release_year,
            "image": self.image(),
        }

class User(models.Model):
    liked_movies = models.ManyToManyField("Movie", through = "UserLike")
    
class UserLike(models.Model):
    user = models.ForeignKey(User)
    movie = models.ForeignKey(Movie)
    is_dislike = models.BooleanField(default = False)