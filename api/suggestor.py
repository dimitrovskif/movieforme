from django.db.models import Count, Q
from . import models

def search_movies(movie_name):
    return models.Movie.objects.filter(name__icontains = movie_name)[:10]
    
def get_liked_movies(user_id):
    return models.Movie.objects.filter(users_liking__id = user_id, userlike__is_dislike = False).all()
    
def like_movie(user_id, movie_id, is_dislike):
    user = models.User.objects.filter(id = user_id).first()
    movie = models.Movie.objects.filter(id = movie_id).first()
    
    like = models.UserLike(user = user, movie = movie, is_dislike = is_dislike)
    like.save()
    
def remove_like_movie(user_id, movie_id):
    models.UserLike.objects.filter(user__id = user_id, movie__id = movie_id).delete()
    
def get_users_who_liked(movie_list):
    movie_id_list = [m.id for m in movie_list]
    return models.User.objects.filter(liked_movies__id__in = movie_id_list).distinct()

def calculate_coef(user1, user2):
    user1_likes = models.UserLike.objects.filter(user__id = user1.id).all()
    user2_likes = models.UserLike.objects.filter(user__id = user2.id).all()
    common_movies = [(like, like2) for like in user1_likes for like2 in user2_likes
        if like.movie.id == like2.movie.id]
    matching_opinions = [movie_opinion for movie_opinion in common_movies
        if movie_opinion[0].is_dislike == movie_opinion[1].is_dislike]
            
    if not len(common_movies):
        return 0
    else:
        coef = len(matching_opinions) / float(len(common_movies))
        return coef