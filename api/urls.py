from django.conf.urls import patterns, include, url
from django.http import JsonResponse
from . import views

urlpatterns = patterns('',
    url(r'^$', views.MainView.as_view()),
    url(r'^search$', views.SearchView.as_view()),
    url(r'^suggest$', views.SuggestView.as_view()),
)
