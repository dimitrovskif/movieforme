from django.views.generic import View
from django.http import JsonResponse
from models import *
import re, itertools

class MainView(View):
    def get(self, *args, **kwargs):
        return JsonResponse({"api_version": "0.0.1"})
        
def format_movie_name(movie_name):
    return re.sub(' +', ' ', movie_name.strip())

class SearchView(View):
    def get(self, *args, **kwargs):
        movie_name = format_movie_name(self.request.GET.get("movie_name", ""))
        results = [m.serialize() for m in 
            Movie.objects.filter(name__istartswith = movie_name)[:10]]
        return JsonResponse({"results": results})
        
class SuggestView(View):
    def parse_movies(self, movie_list):
        """Returns a list of movie names and release years from
        comma separated input. Looks ugly, but the desired behaviour"""
        movie_list = movie_list.split(",")
        parsed_list = []
        for movie_name in movie_list:
            movie_name = format_movie_name(movie_name)
            try:
                movie_year = int(re.match(r".*?\( *([0-9]+) *\)", movie_name).group(1))
                movie_name = movie_name.replace("(%i)" % movie_year, "").strip()
            except AttributeError:
                movie_year = None
            if movie_name != "":
                parsed_list.append({"name": movie_name, "release_year": movie_year})
            
        return parsed_list
        
    def get(self, *args, **kwargs):
        movie_names = self.request.GET.get("movie_names", "")
        movie_names = self.parse_movies(movie_names)
        movie_list = []
        for movie_details in movie_names:
            if movie_details["release_year"]:
                q = Movie.objects.filter(name = movie_details["name"],
                    release_year = movie_details["release_year"])
            else:
                q = Movie.objects.filter(name = movie_details["name"])
            if q.count() > 0: movie_list.append(q[0])
            
        movie_list = [m.serialize() for m in movie_list]
            
        return JsonResponse({"results": movie_list})