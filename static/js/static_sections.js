var React = require("react");
var Router = require("react-router");

var Header = React.createClass({
    render: function(){
        return (
            <div id="header">
                <div className="container">
                    <div id="logo-wrapper">
                        <Router.Link to="homepage">
                            <img src="/images/logo.png" />
                        </Router.Link>
                    </div>
                    <div id="auth-wrapper">
                        <a href="">Log in</a>
                        <a href="" className="signup">Sign up</a>
						<div style={{position: "absolute", right: 0, left: -100, top: 40, background: "#fff"}}>
							Yo
						</div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
});

var Footer = React.createClass({
    render: function(){
        return (
            <div id="footer">
                <div className="container">
                    moviefor.me &copy; {new Date().getFullYear()} Made in Skopje, MK | 
                    <Router.Link to="homepage">About</Router.Link>
                    <Router.Link to="homepage">Privacy</Router.Link>
                    <Router.Link to="homepage">Terms</Router.Link>
                    <Router.Link to="homepage">Cookie policy</Router.Link>
                </div>
            </div>
        );
    }
});

module.exports.Header = Header;
module.exports.Footer = Footer;