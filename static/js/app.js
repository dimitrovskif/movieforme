var React = require("react");
var Router = require("react-router");

var StaticSections = require("./static_sections");
var App = React.createClass({
    render: function(){
        return (
        <div className="app_wrapper">
            <StaticSections.Header />
            <div id="content">
                <Router.RouteHandler />
            </div>
            <StaticSections.Footer />
        </div>
        );
    }
});

$(document).ready(function(){
    var HomePage = require("./homepage");
    var NotFoundPage = require("./not_found");
	var Test = React.createClass({ render: function(){ return  <b>yo</b>;}});
    var routes = (
        <Router.Route name="app" path="/" handler={App}>
            <Router.Route name="homepage" path="/" handler={HomePage} />
			<Router.Route name="wotm8" path="/wotm8" handler={NotFoundPage} />
            <Router.NotFoundRoute handler={NotFoundPage} />
        </Router.Route>
    );
    
    Router.run(routes, function(Handler){
        React.render(<Handler/>, document.body);
    });
});