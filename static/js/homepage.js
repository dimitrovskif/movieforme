var React = require("react");
var Navigation = require("react-router").Navigation;

var AutoCompleteElement = React.createClass({
    onClick: function(){
        this.props.onClick(this);
    },
    render: function(){
        return (
            <li onMouseDown={this.onClick}>
                <div className="pd-5">{this.props.name}</div>
            </li>
        );
    }
});

var AutoCompleteList = React.createClass({
    getInitialState: function(){
        return {search_results: []};
    },
    componentWillReceiveProps: function(new_props){
        $.get("/api/search", {movie_name: new_props.query}, function(data){
            this.setState({search_results: data.results});
        }.bind(this));
    },
    onSuggestionClick: function(e){
        this.props.onSuggestionClick(e);
    },
    render: function(){
        var autocomplete_elements = [];
        this.state.search_results.forEach(function(search_result){
            autocomplete_elements.push(
                <AutoCompleteElement key={search_result.id} name={search_result.decorated_name}
                    onClick={this.onSuggestionClick} />
            );
        }.bind(this));
        
        return (
            <ul style={{display: this.props.isVisible ? "block" : "none"}}
                className="autocomplete-list">{autocomplete_elements}</ul>
        );
    }
});

var LoginRedirection = {
    statics: {
        willTransitionTo: function(transition){
            if(document.cookie.indexOf("log") != -1)
                transition.redirect("/wotm8d");
        }
    }
};


module.exports = React.createClass({
    mixins: [LoginRedirection],
    getInitialState: function(){
        return {search_query: "", show_suggestions: false};
    },
    onInput: function(e){
        var input_field = e.target;
        setTimeout(function(){
            var input_movies = input_field.value.split(",");
            var search_query = input_movies[input_movies.length - 1];
            this.setState({search_query: search_query});
        }.bind(this), 0);
    },
    onSuggestionClick: function(e){
        var search_field = this.refs.search_field.getDOMNode();
        var input_movies = search_field.value.split(",");
        input_movies[input_movies.length - 1] = e.props.name;
        
        search_field.value = input_movies.join(",") + ",";
        this.onInput({target: this.refs.search_field.getDOMNode()});
        setTimeout(function(){
            search_field.focus();
        }.bind(this), 150);
    },
    onFocus: function(e){
        this.setState({show_suggestions: true});
    },
    onBlur: function(e){
        this.setState({show_suggestions: false});
    },
    render: function(){
        return (
        <div>
            <div id="headline">
                <div className="container">
                    <h2>moviefor.me helps you discover movies that you'll love.</h2>
                    <h3>Tell us about your taste, and we will deliver.</h3>
                    <div id="tasteform-container">
                        <form id="tasteform" autoComplete="off">
                            <div id="tasteform-field-box">
                                <div className="pd-5">
                                    <input type="text" ref="search_field" id="tasteform-field" onKeyDown={this.onInput} autoComplete="off"
                                        placeholder="Enter your favourite movies or genres, separated by comma"
                                        onFocus={this.onFocus} onBlur={this.onBlur} />
                                </div>
                            </div>
                            <input type="submit" value="Find me movies" id="tasteform-button"/>
                            <div className="clearfix" />
                            <AutoCompleteList query={this.state.search_query} isVisible={this.state.show_suggestions}
                                onSuggestionClick={this.onSuggestionClick} />
                        </form>
                    </div>
                </div>
            </div>
        </div>);
    }
});